def update( alive, size, iter_n ):
    # calculate neighbour count
    def get_nb_cnt(cell): 
        cc = [-1,0,1]
        nbs = set([ (cell[0]+a,cell[1]+b) for a in cc for b in cc if (a!=b or a!=0)])
        return len( alive & nbs )
    
    # one 'day' in the game
    def play_game():
        new_alive = set()
        grid = [ (row,col) for row in range(size[0]) for col in range(size[1]) ]
        for cell in grid:
            nb_cnt = get_nb_cnt(cell)
            if cell in alive:
                if nb_cnt in [ 2, 3 ]:
                    new_alive.add(cell)
            else:
                if nb_cnt == 3:
                    new_alive.add(cell)
        return new_alive
    
    # simulate generations
    for _ in range( iter_n ):
        alive = play_game()
    return alive

def draw( alive, size ):
    grid = [ [' ']*size[1] for row in range(size[0]) ]      # generate grid
    for row,col in alive:                                   # set alive fields in grid
        grid[row][col] = 'X' 
    res = ['+']+['-']*size[1]+['+\n']                       # top border
    [ res.extend(['|'] + row + ['|\n']) for row in grid ]   # append grid
    res.extend( ['+']+['-']*size[1]+['+'] )                 # bottom border
    return ''.join( res )
