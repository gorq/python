"""
Simulate the stars/planets/satellites motion in 2D space. Every two objects in the universe are attracted by the gravitational force

$$\vec{F_{ij}} = \frac{G m_i m_j}{r_{ij}^2} \frac{\vec{r_{ij}}}{\|r_{ij}\|}.$$ 

The force that acts on the object $i$ is the vectorial sum of the forces induced by all other (massive) objects

$$\vec{F_i} = \sum_{j \neq i} \vec{F_{ij}}$$

Use SI units, don't be concerned with the speed of the code - do not optimize!!!

Write function that takes any number of space objects (named tuples) as arguments (may not be a list of named tuples for any function!!!) plus the size of the time step and number of time steps. For each object it calculates the force caused by other objects (vector sum of attractive forces). It returns the dictionary with name of the object as a key and tuple of lists of coordinates (one list of x, one of y, every time step one item in list). 

Write a decorator that measures number of calling of each function and their runtime of the functions. The information should be printed to standard output in a form "function_name - number_of_calls - time units\n". The decorator takes optional parameter units which allows to specify time units for printing (default is ms). You can implement the unit measurement only for ns, ms, s, min, h and days.

Below is description of all steps for calculating the update. If you are unsure of precise interface see test script for examples of calling the function.
"""

import time # measuring time
from collections import namedtuple

#Define universal gravitation constant
G=6.67408e-11 #N-m2/kg2
SpaceObject = namedtuple('SpaceObject', 'name mass x y vx vy color')
Force = namedtuple('Force', 'fx fy')

## TO DO!!!
def logging(unit='ms'):
    def _fce(fn,lst_of_fns={}):
        def __fce(*args,**kwargs):
            # define dict that will divide time based on units
            time_format = {
                'd'   : 1.1574074*10e-5,
                'h'   : 0.000277777778,
                'min' : 0.0166666667,
                's'   :  1,
                'ms'  : 1000,
                'ns'  : 1000000000
            }
            start=time.time()
            result = fn(*args,**kwargs)
            end=time.time()
            total=end-start
            # increase total time taken of fn so far
            if fn.__name__ in lst_of_fns.keys():
                lst_of_fns[fn.__name__][0] += 1
                lst_of_fns[fn.__name__][1] += total
            else:
                lst_of_fns[fn.__name__] = [1,total]
            # print as we wanted
            print(f'{fn.__name__} - {lst_of_fns[fn.__name__][0]} - {lst_of_fns[fn.__name__][1]*time_format[unit]}{unit}')
            return result
        return __fce
    return _fce



@logging(unit='ms')
def calculate_force(main_obj,*other_objs):
    #input: one of the space objects (indexed as i in below formulas), other space objects (indexed as j, may be any number of them)
    force_x = 0
    force_y = 0
    #calculate force (vector) for each pair (space_object, other_space_object):
    for obj in other_objs:
        if obj == main_obj:
            continue
        distance = (abs(main_obj.x-obj.x)**2+abs(main_obj.y-obj.y)**2)**(1/2)
        f_ij = G*main_obj.mass*obj.mass/distance**2 #|F_ij| = G*m_i*m_j/distance^2
        tmp_x = obj.x - main_obj.x
        force_x += f_ij * tmp_x/distance #F_x = |F_ij| * (other_object.x-space_object.x)/distance
        tmp_y = obj.y - main_obj.y
        force_y += f_ij * tmp_y/distance #analogous for F_y
    #for each coordinate (x, y) it sums force from all other space objects
    return Force(force_x,force_y)  #returns named tuple (see above) that represents x and y components of the gravitational force


@logging(unit='s')
def update_space_object(space_object,force,timestep):
    #here we update coordinates and speed of the object based on the force that acts on it
    #input: space_object we want to update (evolve in time), force (from all other objects) that acts on it, size of timestep
    #returns: named tuple (see above) that contains updated coordinates and speed for given space_object
    #hint:
    #acceleration_x = force_x/mass
    acceleration_x = force.fx/space_object.mass
    #same for y
    acceleration_y = force.fy/space_object.mass
    #speed_change_x = acceleration_x * timestep
    speed_change_x = acceleration_x * timestep
    #same for y
    speed_change_y = acceleration_y * timestep
    #speed_new_x = speed_old_x + speed_change_x
    speed_new_x = space_object.vx + speed_change_x
    #same for y
    speed_new_y = space_object.vy + speed_change_y
    #x_final = x_old + speed_new_x * timestep
    x_final = space_object.x + speed_new_x * timestep
    y_final = space_object.y + speed_new_y * timestep
    return SpaceObject(name=space_object.name, mass=space_object.mass, x=x_final, y=y_final, vx=speed_new_x, vy=speed_new_y, color=space_object.color)


@logging(unit='ms')
def update_motion(timestep,*objects):
    #input: timestep and space objects we want to simulate (as named tuples above)
    #returns: list or tuple with updated objects
    #hint:
    #iterate over space objects, for given space object calculate_force with function above, update
    updated_space_objects = []
    for i in range(len(objects)):
        obj_force   = calculate_force(objects[i],*objects)
        updated_obj = update_space_object(objects[i],obj_force,timestep)
        updated_space_objects.append(updated_obj)

    return updated_space_objects #(named tuple with x and y)


@logging()
def simulate_motion(timestep,iterations,*objects):
    #generator that in every iteration yields dictionary with name of the objects as a key and named tuple SpaceObject as a value
    #input size of timestep, number of timesteps (integer), space objects (any number of them)
    for _ in range(iterations):
        objects = update_motion(timestep,*objects) 
        yield { obj.name: (obj.x,obj.y) for obj in objects }
