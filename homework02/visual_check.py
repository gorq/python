# Run this scipt to have a visual check how the simulation is behaving

from collections import namedtuple

import numpy as np
from IPython.display import HTML
from matplotlib import pyplot as plt
import matplotlib.animation as animation
from matplotlib import rc

import space_motion

ITERS = 10000

def visualize_results(simulation, timestamp, xlim, ylim, *space_objects):
    colors = [o.color for o in space_objects]
    x = [o.x for o in space_objects]
    y = [o.y for o in space_objects]

    fig = plt.figure(figsize=(15, 15))
    plt.xlim(xlim) 
    plt.ylim(ylim) 
    scat = plt.scatter(x, y, c=colors, s=50, label=[x.color for x in space_objects])

    def update_plot(simulation, _, scat):
        scat.set_offsets([list(o) for o in simulation.values()])
        return scat,
    
    interval = timestamp*1000
    ani = animation.FuncAnimation(fig, update_plot, frames=list(simulation),
                                  fargs=(None, scat), interval=interval, repeat=False)

    return ani


def main():
    
    SpaceObject = namedtuple('SpaceObject', 'name mass x y vx vy color')
    
    # Define universal gravitation constant
    G=6.67408e-11 #N-m2/kg2

    #Reference quantities
    m_sun=1.989e30 #mass of the sun [kg]
    m_earth = 5.972e24 #mass of the earth [kg]
    m_venus = 4.867e24 #[kg]
    m_mercury = 3.285e23 #[kg]
    m_mars = 6.39e23 #[kg]
    m_moon = 7.34e22 #[kg]

    au = 1.495e11 #astronomical unit, approximately distance from earth to sun [m]
    speed_earth = 30.0e3    #speed of earth (relative to sun) [m/s]
    speed_venus = 35.02e3   #[m/s]
    speed_mercury = 47.36e3 #[m/s]
    speed_mars = 24.13e3    #[m/s]
    speed_mars = 24.13e3    #[m/s]
    speed_moon = 1.02e3     #[m/s]

    distance_venus = 108.21e9  #[m]
    distance_mercury = 57.91e9 #[m]
    distance_mars = 227.9e9    #[m]
    distance_moon = au+3631e5  #[m]

    day = 24*3600 #[s]
    
    sun = SpaceObject(name='sun', mass=1*m_sun, x=0.0, y=0.0, vx=0.0, vy=0.0, color='yellow')
    earth = SpaceObject(name='earth', mass=1*m_earth, x=au, y=0.0, vx=0.0, vy=speed_earth, color='blue')
    venus = SpaceObject(name='venus', mass=m_venus, x=-distance_venus, y=0.0, vx=0.0, vy=-speed_venus,
                    color='orange')
    #own added planets
    mercury = SpaceObject(name='mercury', mass=m_mercury, x=0, y=distance_mercury, vx=-speed_mercury, vy=0.0, color='grey')
    mars = SpaceObject(name='mars', mass=m_mars, x=0.0, y=-distance_mars, vx=speed_mars, vy=0.0, color='red')
    moon = SpaceObject(name='moon', mass=m_moon, x=distance_moon, y=0.0, vx=0.0, vy=speed_moon+speed_earth, color='grey')

    simulation = space_motion.simulate_motion(0.5*day, ITERS, sun, mercury, venus, moon, earth, mars)
    
    ani = visualize_results(simulation, 0.002, (-2*au, 2*au), (-2*au, 2*au), sun, mercury, venus, moon, earth, mars)
    plt.show()
    HTML(ani.to_jshtml())

if __name__ == '__main__':
    main()
