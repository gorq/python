"""This module serves as a CLI script for image processing and editing.
So far, the supported operations are as below:
    flip: flips image vertically
    mirror: flips image horizontally
    bw: turns image into its greyscale equivalent
    negative: flips the values of all 3 RBG aspects of each pixel
    rotate: rotates an image by 90deg to the right
    sharpen: sharpens an image using high-pass filter
    blur: blurs an image using 'Box blur' kernel
    lighten <0, 100>: inceases brightness by percentage chosen by user
    darken <0, 100>: decreases brightness by percentage chosen by user

Input image file is given as first positional argument.
Output image name is given as second positional argument.
"""

from PIL import Image
import numpy as np
import argparse
import sys
import copy

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--flip',
                        default=False,
                        action='store_true',
                        help='Flip an image vertically.')
arg_parser.add_argument('--mirror',
                        default=False,
                        action='store_true',
                        help='Flip an image horizontally.')
arg_parser.add_argument('--bw',
                        default=False,
                        action='store_true',
                        help='Greyscale an image.')
arg_parser.add_argument('--inverse',
                        default=False,
                        action='store_true',
                        help='Invers pixel values of an image.')
arg_parser.add_argument('--rotate',
                        default=False,
                        action='store_true',
                        help='Rotate an image to the right.')
arg_parser.add_argument('--sharpen',
                        default=False, action='store_true',
                        help='Sharpen an image.')
arg_parser.add_argument('--blur',
                        default=False,
                        action='store_true',
                        help='Blur an image.')
arg_parser.add_argument('--lighten',
                        type=int,
                        help='Lighten an image from 0 to 100%%.',
                        choices=range(0, 101),
                        metavar='<0-100>',
                        default=0)
arg_parser.add_argument('--darken',
                        type=int,
                        help='Darken an image from 0 to 100%%.',
                        choices=range(0, 101),
                        metavar='<0-100>',
                        default=0)
arg_parser.add_argument('INPUT_IMAGE_PATH',
                        type=str,
                        help='Path to source image.')
arg_parser.add_argument('OUTPUT_IMAGE_PATH',
                        type=str,
                        help='Desired output image path.')


# =============================================================================
def rotate(img):
    """Function rotates in image given as it's parameter by 90 degrees to the
    right."""
    # Create new image of swapped dimensions
    res_img = Image.new(img.mode, (img.height, img.width))
    pix_array = np.asarray(img, dtype='uint8')
    # Numpy can only rotate counter-clockwise, so we do that 'k' times
    result = np.rot90(pix_array, k=3)
    return Image.fromarray(np.uint8(result))


# =============================================================================
def flip(img, orientation):
    """Flip an image horizontally/verticaly. If orientaion == 1, it mirrors,
    else (if 0), then it flips the image."""
    pix_array = np.asarray(img, dtype='uint8')
    # Get sum of channels and do it
    pix_array = np.flip(pix_array, orientation)
    return Image.fromarray(pix_array)


# =============================================================================
def bw(img):
    """Function returns edited image into it's greyscale alternative, using
    human-eye adjusted formula "0.212*r, 0.715*g, 0.072*b"""
    pix_array = np.asarray(img, dtype=float)
    greyscale = 0.212, 0.715, 0.072
    # Calculate each channel new values
    pix_array[..., 0] *= greyscale[0]
    pix_array[..., 1] *= greyscale[1]
    pix_array[..., 2] *= greyscale[2]
    # Get sum of channels and do it
    result = (pix_array[..., 0] + pix_array[..., 1] + pix_array[..., 2])
    result = np.clip(result, 0, 255)
    pix_array[..., 0] = pix_array[..., 1] = pix_array[..., 2] = result
    return Image.fromarray(np.uint8(pix_array))


# =============================================================================
def inverse(img):
    """Inverse the values of all pixel, by substracting it's original values
    from 255."""
    pix_array = np.asarray(img, dtype='uint8')
    height, width, _ = pix_array.shape
    temp = np.full((height, width, 3), 255, dtype='uint8')
    temp -= pix_array
    return Image.fromarray(temp)


# =============================================================================
def adjust_brightness(img, value):
    """Adjust brightness of pixels in the image by a percentage given in the
    'value' argument."""
    pix_array = np.asarray(img, dtype=float)
    result = np.clip((pix_array*(1+(value/100))), 0, 255)
    return Image.fromarray(np.uint8(result))


# =============================================================================
def filtr(img, operation):
    """Applies a filter to an image, based on operation. can sharpen/blur."""
    original = np.asarray(img, dtype=float)
    final = original.copy() 

    kernels = {
        'sharpen':  np.array([[0, -.5, 0], [-.5, 3, -.5], [0, -.5, 0]]),
        'blur': np.full((3,3), 1/9, dtype=float)
    }
    
    mask = kernels[operation]
    # Offset of mask == 1
    offset = len(mask)//2
    height, width, _ = original.shape
    for x in range(offset, width-offset):
        for y in range(offset, height-offset):
            # Get the surrounding area and apply mask
            r = original[y-offset:y+offset+1, x-offset:x+offset+1, 0]
            g = original[y-offset:y+offset+1, x-offset:x+offset+1, 1]
            b = original[y-offset:y+offset+1, x-offset:x+offset+1, 2]
            r = np.sum(r*mask)
            g = np.sum(g*mask)
            b = np.sum(b*mask)
            final[y, x] = r, g, b

    final = np.clip(final, 0, 255)
    return Image.fromarray(np.uint8(final))


# Parse arguments
args = arg_parser.parse_args()

# Dictionary of functions and their params
fns = {
    'flip': [flip, [0]],
    'mirror': [flip, [1]],
    'sharpen': [filtr, ['sharpen']],
    'blur': [filtr, ['blur']],
    'lighten': [adjust_brightness, [args.lighten]],
    'darken': [adjust_brightness, [-args.darken]],
    'bw': [bw, []],
    'rotate': [rotate, []],
    'inverse': [inverse, []],
}

# =============================================================================
if __name__ == '__main__':
    src = Image.open(args.INPUT_IMAGE_PATH, mode='r')
    # check file extensions
    if args.OUTPUT_IMAGE_PATH.split('.')[-1] != args.OUTPUT_IMAGE_PATH.split('.')[-1]:
        print(f'File extensions mismatch, terminating...')
        sys.exit()

    # copy the data from the source one
    output = Image.fromarray(np.asarray(src, dtype='uint8'))
    
    # get all the options set, and do them, in order of input
    options = [x.split('-')[-1] for x in sys.argv if x[0] == '-']
    for opt in options:
        if opt not in fns.keys():
            print(f'Unknown option: {opt}')
            arg_parser.print_usage()
            sys.exit()
        output = fns[opt][0](output, *fns[opt][1])

    output.convert(str(src.mode)).save(args.OUTPUT_IMAGE_PATH)
    output.show()

