## Graphical editor

edit_image.py is a simple CLI application for image editing.

## Usage

```bash
python edit_image.py [-h] [--flip] [--mirror] [--bw] [--inverse] [--rotate]
                     [--sharpen] [--blur] [--lighten <0-100>]
                     [--darken <0-100>]
                     INPUT_IMAGE_PATH OUTPUT_IMAGE_PATH
```

You can get a more verbose description of each option and/or argument by running the script with **--help** option enabled.
